import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Item {
    width: 640
    height: 480

    property alias btnPlay: mbtnPlay
    property alias btnStop: mbtnStop
    property alias cbBg: mcbBg
    property alias url: murl

    ColumnLayout {
        id: columnLayout1
        width: 220
        height: 260
        anchors.centerIn: parent

        TextInput {
            id: murl
            x: 52
            width: 80
            height: 36
            text: "http://62.183.34.109:8000/radio107.mp3"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Button {
            id: mbtnPlay
            text: qsTr("Play")
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            id: mbtnStop
            text: qsTr("Stop")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 105
        }

        CheckBox {
            id: mcbBg
            x: 105
            y: 118
            text: qsTr("Играть в фоне")
        }

    }
}

