import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2
import QtMultimedia 5.0


ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    property var appState: Qt.application.state
    property bool manualStopped: false

    onAppStateChanged: {
        console.log("state changed", Qt.application.state)
        if (Qt.application.state == Qt.ApplicationActive) {
                if (!manualStopped) radio.play()
        } else {
            if (!mainForm.cbBg.checked){
                radio.stop()
            }
        }
    }

    MainForm {
        id: mainForm
        anchors.fill: parent

        btnPlay.onClicked: {
            radio.play()
            manualStopped = false
        }
        btnStop.onClicked: {
            radio.stop()
            manualStopped = true
        }
    }

    MediaPlayer {
        id: radio
        source: mainForm.url.text
    }
}
